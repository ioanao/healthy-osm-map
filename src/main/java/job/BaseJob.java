package job;

import org.apache.spark.sql.SparkSession;


public class BaseJob {

    protected static final String WAYS_PATH = "/Users/ioanao/Downloads/andorra-latest.osm.pbf.way.parquet";

    protected static SparkSession sparkSession;

    protected static void initializeSpark(final String appName) {
        sparkSession = SparkSession
                .builder()
                .master("local")
                .appName(appName)
                .config("spark.sql.parquet.binaryAsString", "true")
                .getOrCreate();
    }
}