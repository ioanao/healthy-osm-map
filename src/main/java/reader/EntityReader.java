package reader;

import entity.Node;
import entity.Way;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;
import java.util.Arrays;


public class EntityReader {

    private SparkSession sparkSession;

    public EntityReader(final SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public Dataset<Way> readWays(final String waysPath) {
        return sparkSession
                .read()
                .parquet(waysPath)
                .as(Encoders.bean(Way.class))
                .filter(way -> Arrays.stream(way.getTags()).anyMatch(tag -> tag.getKey().equalsIgnoreCase("highway")));
    }

    public Dataset<Node> readNodes(final String nodesPath) {
        return sparkSession
                .read()
                .parquet(nodesPath)
                .as(Encoders.bean(Node.class));
    }
}