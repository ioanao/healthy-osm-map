package entity;

import java.util.Objects;

public class WayNode {

    private int index;
    private long nodeId;

    public WayNode() {
    }

    public WayNode(int index, long nodeId) {
        this.index = index;
        this.nodeId = nodeId;
    }

    public int getIndex() {
        return index;
    }

    public long getNodeId() {
        return nodeId;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setNodeId(long nodeId) {
        this.nodeId = nodeId;
    }

    @Override
    public String toString() {
        return "Node{nodeId=" + nodeId + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WayNode wayNode = (WayNode) o;
        return index == wayNode.index && nodeId == wayNode.nodeId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(index, nodeId);
    }
}