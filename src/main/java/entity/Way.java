package entity;

import java.io.Serializable;

public class Way implements Serializable {

    private long id;
    private WayNode[] nodes;
    private Tag[] tags;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public WayNode[] getNodes() {
        return nodes;
    }

    public void setNodes(WayNode[] nodes) {
        this.nodes = nodes;
    }

    public Tag[] getTags() {
        return tags;
    }

    public void setTags(Tag[] tags) {
        this.tags = tags;
    }
}