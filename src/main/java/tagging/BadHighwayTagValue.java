package tagging;

import entity.Tag;
import entity.Way;
import job.BaseJob;
import reader.EntityReader;
import tagging.entity.WayIdHighwayTagPair;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


public class BadHighwayTagValue extends BaseJob {

    private static final List<String> CORE_WAYS =
            Arrays.asList("motorway", "motorway_link", "trunk", "trunk_link", "primary", "primary_link", "secondary",
                    "secondary_link", "tertiary", "tertiary_link", "unclassified", "residential", "service", "services",
                    "living_street", "pedestrian", "track", "bus_guideway", "raceway", "road", "steps", "footway",
                    "construction", "proposed", "cycleway", "bridleway", "rest_area", "path");
    private static final String HIGHWAY = "highway";


    public static void main(final String[] args) {
        initializeSpark("BadHighwayTagValue");

        final EntityReader reader = new EntityReader(sparkSession);

        final Dataset<Way> ways = reader.readWays(WAYS_PATH);

        ways.map(way -> new WayIdHighwayTagPair(way.getId(), getWrongTag(way.getTags())), Encoders.bean(WayIdHighwayTagPair.class))
                .filter(wayIdHighwayTagPair -> wayIdHighwayTagPair.getTag() != null)
                .write()
                .csv("/user/ioanao/healthy/wrong_tagged_ways");
    }

    private static String getWrongTag(final Tag[] tags) {
        Optional<Tag> highway = Arrays.stream(tags)
                .filter(tag -> tag.getKey().equalsIgnoreCase(HIGHWAY))
                .findFirst();

        if (highway.isPresent()) {
            if (!CORE_WAYS.contains(highway.get().getValue())) {
                return highway.get().getValue();
            } else {
                return null;
            }
        }
        return null;
    }
}