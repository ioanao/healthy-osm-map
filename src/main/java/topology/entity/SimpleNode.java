package topology.entity;


import entity.Tag;

public class SimpleNode {

    private boolean firstOrLast;
    private int index;
    private long simpleNodeId;
    private Tag[] tags;
    private long wayId;


    public SimpleNode(long wayId, int index, long simpleNodeId, boolean firstOrLast, final Tag[] tags) {
        this.wayId = wayId;
        this.index = index;
        this.simpleNodeId = simpleNodeId;
        this.firstOrLast = firstOrLast;
        this.tags = tags;
    }

    public long getWayId() {
        return wayId;
    }

    public void setWayId(long wayId) {
        this.wayId = wayId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public long getSimpleNodeId() {
        return simpleNodeId;
    }

    public void setSimpleNodeId(long simpleNodeId) {
        this.simpleNodeId = simpleNodeId;
    }

    public boolean isFirstOrLast() {
        return firstOrLast;
    }

    public void setFirstOrLast(boolean firstOrLast) {
        this.firstOrLast = firstOrLast;
    }

    public Tag[] getTags() {
        return tags;
    }

    public void setTags(Tag[] tags) {
        this.tags = tags;
    }
}