package topology.entity;

import entity.Tag;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


public class Edge implements Serializable {

    private long dst;
    private long id;
    private String oneway;
    private String nodes;
    private long src;
    private List<Tag> tags;


    public Edge() {
    }

    public Edge(long id, long src, long dst, final List<Tag> tags) {
        this.id = id;
        this.src = src;
        this.dst = dst;
        this.tags = tags;
        setOneway(tags);
    }

    public Edge(long id, long src, long dst) {
        this.id = id;
        this.src = src;
        this.dst = dst;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSrc() {
        return src;
    }

    public void setSrc(long src) {
        this.src = src;
    }

    public long getDst() {
        return dst;
    }

    public void setDst(long dst) {
        this.dst = dst;
    }

    public String getNodes() {
        return nodes;
    }

    public void setNodes(String nodes) {
        this.nodes = nodes;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(final List<Tag> tags) {
        this.tags = tags;
    }

    public String getOneway() {
        return oneway;
    }

    public void setOneway(String oneway) {
        this.oneway = oneway;
    }

    private void setOneway(List<Tag> tags) {
        Optional<Tag> highway = tags.stream()
                .filter(tag -> tag.getKey().equalsIgnoreCase("oneway"))
                .findFirst();

        if (highway.isPresent()) {
            String onewayTag = highway.get().getValue();
            if (onewayTag.equalsIgnoreCase("yes") || onewayTag.equalsIgnoreCase("1") || onewayTag.equalsIgnoreCase("true")) {
                this.oneway = "true";
            } else if (onewayTag.equalsIgnoreCase("-1") || onewayTag.equalsIgnoreCase("reverse")) {
                this.oneway = "reversed";
            } else {
                this.oneway = "false";
            }
        } else {
            this.oneway = "false";
        }
    }
}