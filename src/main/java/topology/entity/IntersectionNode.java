package topology.entity;

import entity.Tag;
import java.util.List;


public class IntersectionNode {

    private final long wayId;

    private final long nodeId;

    private int index;

    private List<Tag> tags;

    public IntersectionNode(long wayId, long nodeId, int index, List<Tag> tags) {
        this.wayId = wayId;
        this.nodeId = nodeId;
        this.index = index;
        this.tags = tags;
    }

    public long getWayId() {
        return wayId;
    }

    public long getNodeId() {
        return nodeId;
    }

    public int getIndex() {
        return index;
    }

    public List<Tag> getTags() {
        return tags;
    }
}