package topology.entity;

import java.io.Serializable;

public class Vertex implements Serializable {

    private long id;

    public Vertex() {
    }

    public Vertex(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}