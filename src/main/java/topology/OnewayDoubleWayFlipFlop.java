package topology;

import entity.Way;
import job.BaseJob;
import org.apache.spark.sql.*;
import reader.EntityReader;
import static org.apache.spark.sql.functions.col;
import static topology.util.Column.*;
import static topology.util.TopologyUtil.*;


/**
 * Find patterns like [one way - double way - one way] (-> = ->)
 */
public class OnewayDoubleWayFlipFlop extends BaseJob {

    private static final String ID1 = "id1";
    private static final String ID1_SRC = "id1_src";
    private static final String ID1_DST = "id1_dst";
    private static final String ID2 = "id2";
    private static final String ID2_SRC = "id2_src";
    private static final String ID2_DST = "id2_dst";
    private static final String ID3 = "id3";

    public static void main(final String[] args) {
        initializeSpark("OnewayDoubleWayFlipFlop");

        final EntityReader reader = new EntityReader(sparkSession);

        // read ways -> dataset[(id, nodes, tags)]
        final Dataset<Way> ways = reader.readWays(args[0]);

        // build edges by cutting the ways at each intersections
        final Dataset<Row> edges = waysToEdges(ways);

        // get nodes that intersects at least 3 edges -> dataset[(value)]
        final Dataset<Row> complexIntersections = getAtLeast3EdgeIntersections(edges);

        final Dataset<Row> doubleWays = edges
                .where(col(ONEWAY_COL).equalTo("false"))
                .join(complexIntersections, complexIntersections.col(VALUE).equalTo(edges.col(EDGE_DEST_COL)), LEFT_JOIN)
                .where(col(VALUE).isNull())
                .select(col(EDGE_ID_COL).as(ID1), col(EDGE_SRC_COL).as(ID1_SRC), col(EDGE_DEST_COL).as(ID1_DST));

        final Dataset<Row> tuple2 = doubleWays
                .join(edges, edges.col(EDGE_SRC_COL).equalTo(doubleWays.col(ID1_DST)))
                .where(col(ONEWAY_COL).equalTo("true"))
                .select(col(ID1), col(ID1_SRC), col(ID1_DST), col(EDGE_ID_COL).as(ID2), col(EDGE_SRC_COL).as(ID2_SRC),
                        col(EDGE_DEST_COL).as(ID2_DST));

        final Dataset<Row> tuple2WithoutIntersections = tuple2
                .join(complexIntersections, complexIntersections.col(VALUE).equalTo(tuple2.col(ID2_DST)), LEFT_JOIN)
                .where(col(VALUE).isNull())
                .select(col(ID1), col(ID1_SRC), col(ID1_DST), col(ID2), col(ID2_SRC), col(ID2_DST));

        final Dataset<Row> tuple3 = tuple2WithoutIntersections
                .join(edges, edges.col(EDGE_SRC_COL).equalTo(tuple2WithoutIntersections.col(ID2_DST)))
                .where(col(ONEWAY_COL).equalTo("false"))
                .select(col(ID1), col(ID1_SRC), col(ID1_DST), col(ID2), col(ID2_SRC), col(ID2_DST),
                        col(EDGE_ID_COL).as(ID3), col(EDGE_SRC_COL).as("id3_src"), col(EDGE_DEST_COL).as("id3_dst"));

        tuple3.write().csv("/Users/ioanao/Downloads/output");
    }
}