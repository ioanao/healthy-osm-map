package topology.util;

public class Column {

    public static final String ONEWAY_COL = "oneway";
    public static final String EDGE_DEST_COL = "dst";
    public static final String EDGE_SRC_COL = "src";
    public static final String EDGE_ID_COL = "id";
    public static final String VALUE = "value";
}