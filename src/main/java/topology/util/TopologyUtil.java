package topology.util;

import entity.Tag;
import entity.Way;
import entity.WayNode;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import topology.entity.Edge;
import topology.entity.IntersectionNode;
import topology.entity.SimpleNode;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static org.apache.spark.sql.functions.col;


public class TopologyUtil {

    public static final String LEFT_JOIN = "left";

    public static final List<String> IMPORTANT_WAYS = Arrays.asList("motorway", "motorway_link", "primary",
            "primary_link", "secondary", "secondary_link", "tertiary", "tertiary_link", "trunk", "trunk_link",
            "residential", "road", "service", "unclassified");

    public static boolean isImportantWay(final List<Tag> tags) {
        final Optional<Tag> highway = tags.stream()
                .filter(tag -> tag.getKey().equalsIgnoreCase("highway"))
                .findFirst();
        return highway.isPresent() && IMPORTANT_WAYS.contains(highway.get().getValue());
    }

    public static Dataset<Row> waysToEdges(final Dataset<Way> ways) {

        // get all way nodes -> dataset[(firstOrLast, index, simpleNodeId, wayId)]
        final Dataset<SimpleNode> allWayNodes = getAllWayNodes(ways);

        // get all nodes that are intersections (link at least two ways) -> dataset[(nodeId)]
        final Dataset<Row> intersections = getIntersections(allWayNodes);

        // build edges by cutting the ways at each intersections -> dataset[(dst,id,oneway,nodes,src,tags)]
        return buildEdges(allWayNodes, intersections);
    }

    public static Dataset<SimpleNode> getAllWayNodes(final Dataset<Way> ways) {
        return ways.flatMap(way -> Arrays.stream(way.getNodes())
                        .map(node -> wayNodeToSimpleNode(way, node))
                        .collect(Collectors.toList()).iterator(),
                Encoders.bean(SimpleNode.class));
    }

    private static SimpleNode wayNodeToSimpleNode(final Way way, final WayNode wayInnerNode) {
        final boolean isStartOrEndNode = wayInnerNode.getIndex() == 0 || wayInnerNode.getIndex() == way.getNodes().length - 1;
        return new SimpleNode(way.getId(), wayInnerNode.getIndex(), wayInnerNode.getNodeId(), isStartOrEndNode, way.getTags());
    }

    public static Dataset<Row> getIntersections(final Dataset<SimpleNode> allWayNodes) {
        return allWayNodes
                .groupBy("simpleNodeId").count()
                .filter("count > 1")
                .select(col("simpleNodeId").as("nodeId"));
    }

    public static Dataset<Row> buildEdges(final Dataset<SimpleNode> allWayNodes, final Dataset<Row> intersections) {
        return allWayNodes
                .join(intersections, intersections.col("nodeId").equalTo(allWayNodes.col("simpleNodeId")), LEFT_JOIN)
                .filter(col("nodeId").isNotNull().or(col("firstOrLast").equalTo(Boolean.TRUE)))
                .select(col("wayId"), col("simpleNodeId"), col("index"), col("tags"))
                .groupByKey(row -> row.getLong(0), Encoders.LONG())
                .flatMapGroups(TopologyUtil::buildEdgesForWay, Encoders.bean(Edge.class))
                .toDF();
    }

    public static Iterator<Edge> buildEdgesForWay(long wayId, Iterator<Row> iterator) {
        final List<IntersectionNode> nodes = new ArrayList<>();
        while (iterator.hasNext()) {
            final Row row = iterator.next();
            final List<Row> tags = row.getList(3);
            final List<Tag> tagList = tags
                    .stream()
                    .map(tag -> new Tag(tag.getString(0), tag.getString(1)))
                    .collect(toList());
            if (isImportantWay(tagList)) {
                nodes.add(new IntersectionNode(row.getAs("wayId"), row.getAs("simpleNodeId"), row.getAs("index"), tagList));
            }
        }

        List<Edge> collect = new ArrayList<>();
        if (nodes.size() == 1) {
            collect.add(new Edge(wayId, nodes.get(0).getNodeId(), nodes.get(0).getNodeId(), nodes.get(0).getTags()));
        } else {
            final List<IntersectionNode> sorted = nodes.stream()
                    .sorted(Comparator.comparing(IntersectionNode::getIndex))
                    .collect(Collectors.toList());

            collect = range(0, sorted.size() - 1)
                    .mapToObj(i -> new Edge(wayId, sorted.get(i).getNodeId(), sorted.get(i + 1).getNodeId(), nodes.get(0).getTags()))
                    .collect(toList());
        }

        return collect.iterator();
    }

    public static Dataset<Row> getAtLeast3EdgeIntersections(final Dataset<Row> edges) {
        return edges
                .flatMap(edge -> Arrays.asList(edge.getLong(edge.fieldIndex("src")), edge.getLong(edge.fieldIndex("dst"))).iterator(),
                        Encoders.LONG())
                .groupBy("value").count()
                .where("count > 2")
                .select("value");
    }
}