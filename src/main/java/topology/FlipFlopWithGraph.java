package topology;

import entity.Tag;
import entity.Way;
import job.BaseJob;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.graphframes.GraphFrame;
import org.spark_project.guava.collect.ImmutableList;
import reader.EntityReader;
import topology.entity.Edge;
import topology.entity.IntersectionNode;
import topology.entity.SimpleNode;
import topology.entity.Vertex;
import java.util.*;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.types.DataTypes.LongType;
import static org.apache.spark.sql.types.DataTypes.createStructField;

//TODO revisit
public class FlipFlopWithGraph extends BaseJob {

    public static void main(final String[] args) {
        initializeSpark("FlipFlopWithGraph");

        final EntityReader reader = new EntityReader(sparkSession);

        // read ways
        final Dataset<Way> ways = reader.readWays(args[0]);

        // for each node build a dataset with the following structure [firstOrLast, index, simpleNodeId, wayId]
        final Dataset<SimpleNode> allWayNodes = ways
                .flatMap(way -> Arrays.stream(way.getNodes())
                        .map(node -> new SimpleNode(way.getId(), node.getIndex(), node.getNodeId(),
                                (node.getIndex() == 0 || node.getIndex() == way.getNodes().length - 1), way.getTags()))
                        .collect(Collectors.toList()).iterator(), Encoders.bean(SimpleNode.class));

        // dataset with [nodeId] structure
        final Dataset<Row> intersections = getIntersections(allWayNodes);

        final Dataset<Row> edges = buildEdges(allWayNodes, intersections);

        final Dataset<Row> vertices = edges
                .flatMap(edge -> Arrays.asList(edge.getLong(edge.fieldIndex("src")), edge.getLong(edge.fieldIndex("dst"))).iterator(),
                        Encoders.LONG())
                .distinct()
                .map(Vertex::new, Encoders.bean(Vertex.class))
                .toDF();

        final GraphFrame graph = new GraphFrame(vertices, edges);

   /*     final Dataset<Row> trueIntersections = allWayNodes
                .groupBy("simpleNodeId").count()
                //.filter("count > 2")
                .select(col("simpleNodeId").as("nodeId"), col("count"));*/

        // persists graph related information
        //writeGraphInfos(graph);

        final Dataset<Row> edge3Tuples = graph.find("(n1)-[e1]->(n2); (n2)-[e2]->(n3); (n3)-[e3]->(n4)")
                .filter(col("e1.oneway").equalTo("true").and(col("e2.oneway").equalTo("false")).and(col("e3.oneway").equalTo("true")))
                .distinct()
                .where((col("e1.src").notEqual(col("e2.dst"))).and(col("e3.dst").notEqual(col("e2.src"))))
                .select(col("e1.id").as("id1"), col("e1.src").as("id1_src"), col("e1.dst").as("id1_dst"),
                col("e2.id").as("id2"), col("e2.src").as("id2_src"), col("e2.dst").as("id2_dst"),
                col("e3.id").as("id3"), col("e3.src").as("id3_src"), col("e3.dst").as("id3_dst"));

        final Dataset<Row> where = edge3Tuples
                .join(intersections, intersections.col("nodeId").equalTo(edge3Tuples.col("id2_dst")), "left")
                .where(col("nodeId").isNull());

        /*final Dataset<Row>  tupledWithNoIntersectionAsSecondNode = edge3Tuples
                .join(intersections, intersections.col("nodeId").equalTo(edge3Tuples.col("e2.dst")), "left")
                //.where(col("nodeId").isNull())
                .select(col("e1.id").as("id1"), col("e1.src").as("id1_src"), col("e1.dst").as("id1_dst"),
                col("e2.id").as("id2"), col("e2.src").as("id2_src"), col("e2.dst").as("id2_dst"),
                col("e3.id").as("id3"), col("e3.src").as("id3_src"), col("e3.dst").as("id3_dst"));
                //col("nodeId").as("secondNode"));*/

        /*final Dataset<Row> select = tupledWithNoIntersectionAsSecondNode
                .join(intersections, intersections.col("nodeId").equalTo(tupledWithNoIntersectionAsSecondNode.col("id2_src")), "left")
                .where(col("nodeId").isNull().and(col("secondNode").isNull()));*/
        //select.write().csv("/Users/ioanao/Downloads/output");
        where.write().csv("/user/ioanao/healthy/tuples");
    }

    private static Dataset<Row> getIntersections(final Dataset<SimpleNode> allWayNodes) {
        return allWayNodes
                .groupBy("simpleNodeId").count()
                .filter("count > 1")
                .select(col("simpleNodeId").as("nodeId"));   //4607
    }

    private static Dataset<Row> buildEdges(final Dataset<SimpleNode> allWayNodes, final Dataset<Row> intersections) {
        return allWayNodes
                .join(intersections, intersections.col("nodeId").equalTo(allWayNodes.col("simpleNodeId")), "left")
                .filter(col("nodeId").isNotNull().or(col("firstOrLast").equalTo(Boolean.TRUE)))
                .select(col("wayId"), col("simpleNodeId"), col("index"), col("tags"))
                .groupByKey(row -> row.getLong(0), Encoders.LONG())
                .flatMapGroups(FlipFlopWithGraph::buildEdgesForWay, Encoders.bean(Edge.class))
                .toDF();
    }

    private static Iterator<Edge> buildEdgesForWay(long wayId, Iterator<Row> iterator) {
        final List<IntersectionNode> nodes = new ArrayList<>();
        while (iterator.hasNext()) {
            final Row row = iterator.next();
            final List<Row> tags = row.getList(3);
            final List<Tag> tagList = tags
                    .stream()
                    .map(tag -> new Tag(tag.getString(0), tag.getString(1)))
                    .collect(toList());
            nodes.add(new IntersectionNode(row.getAs("wayId"), row.getAs("simpleNodeId"), row.getAs("index"), tagList));
        }

        List<Edge> collect = new ArrayList<>();
        if (nodes.size() == 1) {
            collect.add(new Edge(wayId, nodes.get(0).getNodeId(), nodes.get(0).getNodeId(), nodes.get(0).getTags()));
        } else {
            final List<IntersectionNode> sorted = nodes.stream()
                    .sorted(Comparator.comparing(IntersectionNode::getIndex))
                    .collect(Collectors.toList());

            collect = range(0, sorted.size() - 1)
                    .mapToObj(i -> new Edge(wayId, sorted.get(i).getNodeId(), sorted.get(i + 1).getNodeId(), nodes.get(0).getTags()))
                    .collect(toList());
        }

        return collect.iterator();
    }

    private static Dataset<Row> buildVertices(final Dataset<SimpleNode> nodes) {
        return nodes
                .map(node -> new Vertex(node.getSimpleNodeId()), Encoders.bean(Vertex.class))
                .toDF();
    }

    private static void writeFilteredInfo(final Dataset<Row> rows1, final Dataset<Row> rows) {
        final StructType schema = DataTypes.createStructType(
                new StructField[]{
                        createStructField("COUNT_FILTERED", LongType, false),
                        createStructField("COUNT_ALL", LongType, false)
                });
        final List<Row> rowList = ImmutableList.of(RowFactory.create(0L, rows.count()));
        final Dataset<Row> data = sparkSession.createDataFrame(rowList, schema);
        data.write().csv("/user/ioanao/healthy/count");
    }

    private static void writeGraphInfos(final GraphFrame graph) {
        final StructType schema = DataTypes.createStructType(
                new StructField[]{
                        createStructField("EDGE_COUNT", LongType, false),
                        createStructField("VERTEX_COUNT", LongType, false)
                });
        final List<Row> rowList = ImmutableList.of(RowFactory.create(graph.edges().count(), graph.vertices().count()));
        final Dataset<Row> data = sparkSession.createDataFrame(rowList, schema);
        data.write().csv("/user/ioanao/healthy/graph_no_of_edges_end_vertices");
    }
}