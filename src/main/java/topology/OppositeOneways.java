package topology;

import entity.Way;
import job.BaseJob;
import org.apache.spark.sql.*;
import reader.EntityReader;
import static org.apache.spark.sql.functions.col;
import static topology.util.Column.*;
import static topology.util.TopologyUtil.*;


/**
 * Find pattern like [one way-one way] (-> <-)
 */
public class OppositeOneways extends BaseJob {

    private static final String ID1 = "id1";
    private static final String ID1_SRC = "id1_src";
    private static final String ID1_DST = "id1_dst";

    public static void main(final String[] args) {
        initializeSpark("OppositeOneways");

        final EntityReader reader = new EntityReader(sparkSession);

        // read ways -> dataset[(id, nodes, tags)]
        final Dataset<Way> ways = reader.readWays(WAYS_PATH);

        // build edges by cutting the ways at each intersections -> dataset[(dst,id,oneway,nodes,src,tags)]
        final Dataset<Row> edges = waysToEdges(ways);

        // get nodes that intersects at least 3 edges -> dataset[(value)]
        final Dataset<Row> complexIntersections = getAtLeast3EdgeIntersections(edges);

        final Dataset<Row> first = edges
                .where(col(ONEWAY_COL).equalTo("true"))
                .join(complexIntersections, complexIntersections.col(VALUE).equalTo(edges.col(EDGE_DEST_COL)), LEFT_JOIN)
                .where(col(VALUE).isNull())
                .select(col(EDGE_ID_COL).as(ID1), col(EDGE_ID_COL).as(ID1_SRC), col(EDGE_DEST_COL).as(ID1_DST));

        final Dataset<Row> firstAndSecond = first
                .join(edges, edges.col(EDGE_DEST_COL).equalTo(first.col(ID1_DST)))
                .where(col(ONEWAY_COL).equalTo("true").and(col(EDGE_ID_COL).notEqual(col(ID1))))
                .select(col(ID1), col(ID1_SRC), col(ID1_DST), col(EDGE_ID_COL), col(EDGE_SRC_COL), col(EDGE_DEST_COL));

        firstAndSecond.write().csv("/user/ioanao/healthy/oppositeOneways");
    }
}